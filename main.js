function factorial(num) {
  if (num === 0) {
    return 1;
  } else {
    return num * factorial(num - 1);
  }
}

let input = prompt("Введіть число:");
let num = parseInt(input);

if (isNaN(num)) {
  alert("Введіть число!");
} else {
  let result = factorial(num);
  alert("Факторіал " + num + " дорівнює " + result);
}